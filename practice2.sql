--[SUPPLEMENTARY ACTIVITY 2]

--create a new database called course_db;
CREATE DATABASE course_db;

--drop the course_db databse;
DROP DATABASE course_db;

--recreate the database;
CREATE DATABASE course_db;

--select the course_db database;
USE course_db;
--create 3 tables
CREATE TABLE students(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	email VARCHAR(50),
	PRIMARY KEY (id)
);

CREATE TABLE subjects(
	id INT NOT NULL AUTO_INCREMENT,
	course_name VARCHAR(100) NOT NULL,
	schedule VARCHAR(100) NOT NULL,
	instructor VARCHAR(100) NOT NULL,
	PRIMARY KEY (id)
);
CREATE TABLE enrollments(
	id INT NOT NULL AUTO_INCREMENT,
	student_id INT NOT NULL,
	subject_id INT NOT NULL,
	datetime_created TIMESTAMP NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_enrollments_student_id
		FOREIGN KEY (student_id) REFERENCES students(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_enrollments_subject_id
		FOREIGN KEY (subject_id) REFERENCES subjects(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT	
);

--create records for each table
INSERT INTO students (username, password, full_name, email) VALUES ("justine123", "password123", "justine dg", "justine@mail.com");
INSERT INTO subjects (course_name, schedule, instructor) VALUES ("data structures", "8AM-10PM", "Mr.santos");
INSERT INTO enrollments (student_id, subject_id, datetime_created) VALUES (1,1, '2023-05-03 20:23:50');