--[SUPPLEMENTARY ACTIVITY]

--delete the reviews table

DROP TABLE reviews;

--Create a new reviews table
CREATE TABLE reviews(
	id INT NOT NULL AUTO_INCREMENT,
	review VARCHAR(500) NOT NULL,
	datetime_created DATETIME NOT NULL,
	rating INT NOT NULL,
	PRIMARY KEY (id)
);

--create 5 review records
INSERT INTO reviews(review, datetime_created, rating) VALUES ("The songs are lit!", "2023-05-03", 5), ("Some songs are not good", "2023-05-03", 2), ("The songs are okay", "2023-05-03", 3),("I didn't like the songs compared to his previous album", "2023-05-03", 1), ("The songs are wild!", "2023-05-03", 4);  
  
   
--display all review records
SELECT * FROM reviews;

-- display all review records with the rating of 5
SELECT * FROM reviews WHERE rating = 5;

-- display all review records with the rating of 1
SELECT * FROM reviews WHERE rating = 1;

-- update all review ratings to 5
UPDATE reviews SET rating = 5;